packer {
  required_plugins {
    docker = {
      version = ">= 1.0.8"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
  image  = "ubuntu:jammy"
  commit = true
}

build {
  name = "postgres-packer"
  sources = [
    "source.docker.ubuntu"
  ]
  post-processor "docker-tag" {
      repository = "gitlab/sabavivek/postgreswithpacker"
      tag = ["1.0"]
  }
}

